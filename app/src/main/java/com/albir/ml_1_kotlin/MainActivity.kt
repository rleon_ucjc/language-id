package com.albir.ml_1_kotlin

import android.annotation.SuppressLint
import android.app.Activity
import android.icu.lang.UCharacter.IndicPositionalCategory.BOTTOM
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log
import android.view.Gravity.END
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.common.io.Resources.getResource
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.ktx.Firebase
//import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.firebase.ml.naturallanguage.FirebaseNaturalLanguage
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.google.firebase.remoteconfig.ktx.get
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfigSettings
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*
import java.lang.RuntimeException
import java.util.ArrayList
import java.util.Locale
import com.google.firebase.perf.FirebasePerformance;
import com.google.firebase.perf.metrics.AddTrace
import com.google.firebase.perf.metrics.Trace;


class MainActivity : AppCompatActivity() {
    private var outputText: TextView? = null
    private var welcomeTextView: TextView? = null
    val crashlytics = FirebaseCrashlytics.getInstance()
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private lateinit var remoteConfig: FirebaseRemoteConfig



    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        outputText = findViewById(R.id.outputText)
        welcomeTextView = findViewById(R.id.welcomeTextView)


        // Para Analytics
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)

        // Para Remote Config
        remoteConfig = Firebase.remoteConfig
        val configSettings = remoteConfigSettings {
            minimumFetchIntervalInSeconds = 3600
        }
        remoteConfig.setConfigSettingsAsync(configSettings)
        remoteConfig.setDefaultsAsync(R.xml.remote_config_defaults)

        fetchWelcome()

        buttonIdLanguage.setOnClickListener {
            val input = inputText.text?.toString()
            input?.let {
                inputText.text?.clear()
                identifyLanguage(it)
                fetchWelcome()
            }
        }

        buttonIdAll.setOnClickListener {
            val input = inputText.text?.toString()
            input?.let {
                inputText.text?.clear()
                identifyPossibleLanguages(input)
                fetchWelcome()
            }
        }


    }


    @AddTrace(name = "identifyPossibleLanguages", enabled = true)
    private fun identifyPossibleLanguages(inputText: String) {

        val trace = FirebasePerformance.getInstance().newTrace("Identifico Posibles Lenguajes")
        trace.start()
        trace.putAttribute("Identificación Lenguaje","A")
        val experimentValue = trace.getAttribute("Identificación Lenguaje")
        welcomeTextView?.text = experimentValue

        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.METHOD,"identifyPossibleLanguages")
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LEVEL_START,bundle)

        val languageIdentification = FirebaseNaturalLanguage.getInstance()
            .languageIdentification


        Thread.sleep(2000)

        languageIdentification.identifyPossibleLanguages(inputText)
            .addOnSuccessListener { identifyLanguages ->
                val detectedLanguages = ArrayList <String>(identifyLanguages.size)

                for(language in identifyLanguages){
                    detectedLanguages.add(
                        String.format(Locale.US,"%s (%3f)", language.languageCode, language.confidence)
                    )
                }
                outputText?.append(
                    String.format(Locale.US, "\n%s - [%s]",inputText,TextUtils.join(",", detectedLanguages))
                )
            }
            .addOnFailureListener { e ->
                Log.e(TAG,"Language Identification error.",e)
                crashlytics.log(e.toString())
                FirebaseCrashlytics.getInstance().recordException(e)
            }

        trace.getAttribute("Identificación Lenguaje")
        trace.stop()
    }




    @AddTrace(name = "identifyLanguages", enabled = true)
    private fun identifyLanguage(inputText: String) {
        val myTrace = FirebasePerformance.getInstance().newTrace("Identifico Lenguaje")
        myTrace.start()

        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.METHOD,"identifyLanguage")
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_TO_WISHLIST,bundle)

        val languageIdentification = FirebaseNaturalLanguage.getInstance().languageIdentification

        languageIdentification.identifyLanguage(inputText)
            .addOnSuccessListener { s ->
                outputText?.append(
                    String.format(Locale.US,"\n%s - %s",inputText,s)
                )
            }
            .addOnFailureListener { e ->
                Log.e(TAG,"Language identification error.",e)
                crashlytics.log(e.toString())
                FirebaseCrashlytics.getInstance().recordException(e)
            }

        myTrace.stop()
    }


    @AddTrace(name = "fetchWelcomes", enabled = true)
   @RequiresApi(Build.VERSION_CODES.M)
   private fun fetchWelcome(){
        val myTrace = FirebasePerformance.getInstance().newTrace("Descarga Parámetros")
        myTrace.start()

        welcomeTextView?.text = remoteConfig[LOADING_PHRASE_CONFIG_KEY].asString()

        remoteConfig.fetchAndActivate()
            .addOnCompleteListener(this){task ->
                if(task.isSuccessful){
                    val updated = task.result
                    Log.d(TAG,"Configuración de los Parámetros actualizados: $updated")
                    Toast.makeText(this,"Descarga de parámetros con éxito.",Toast.LENGTH_SHORT)
                        .show()
                } else {
                    Toast.makeText(this,"No ha se ha podido realizar la descarga de parámetros.",Toast.LENGTH_SHORT)
                        .show()
                }
                displayWelcomeMessage()
            }
       myTrace.stop()
    }


    @AddTrace(name = "displayWecomeMessages", enabled = true)
    @RequiresApi(Build.VERSION_CODES.M)
    private fun displayWelcomeMessage(){
        val myTrace = FirebasePerformance.getInstance().newTrace("Rendering the message")
        myTrace.start()
        var welcomeMessage = remoteConfig[WELCOME_MESSAGE_KEY].asString()
        //var color_texto = remoteConfig[COLOR_TEXT].asLong()
        //var welcomeMessage = remoteConfig.getString(WELCOME_MESSAGE_KEY)
        Log.d(TAG,"Mensaje recibido: $welcomeMessage")

        val change_color:Boolean = remoteConfig[HAY_COLOR].asBoolean()
        if(change_color){
            var micolor = getColor(R.color.colorBlanco)
            welcomeTextView?.setTextColor(micolor)
            micolor = getColor(R.color.colorPrimaryDark)
            welcomeTextView?.setBackgroundColor(micolor)
        }

        welcomeTextView?.text = welcomeMessage

        myTrace.stop()
    }

    companion object {
        private const val TAG = "MainActivity"

        private const val LOADING_PHRASE_CONFIG_KEY = "loading_phrase"
        private const val WELCOME_MESSAGE_KEY = "welcome_message"
        private const val HAY_COLOR = "hay_color"
    }
}
